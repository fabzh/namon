# NAMON

## Prerequisites

PHP version >= 5.3.0

## Tests

### Main station
```shell
# ./getData.php -i 70:ee:50:00:02:20 -f time_utc
# ./getData.php -i 70:ee:50:00:02:20 -f firmware
# ./getData.php -i 70:ee:50:00:02:20 -f wifi_status
```

### Outdoor module
```shell
# ./getData.php -i 02:00:00:13:08:c4 -f time_utc
# ./getData.php -i 02:00:00:13:08:c4 -f firmware
# ./getData.php -i 02:00:00:13:08:c4 -f battery_percent
# ./getData.php -i 02:00:00:13:08:c4 -f rf_status
```

### Rain gauge
```shell
# ./getData.php -i 05:00:00:00:0f:98 -f time_utc
# ./getData.php -i 05:00:00:00:0f:98 -f firmware
# ./getData.php -i 05:00:00:00:0f:98 -f battery_percent
# ./getData.php -i 05:00:00:00:0f:98 -f rf_status
```