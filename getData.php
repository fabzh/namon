#!/usr/bin/php
<?php
/**
* Example of Weather station API
* If you need more details, please take a glance at https://dev.netatmo.com/doc
*/

require_once ('src/Netatmo/autoload.php');
//require_once 'utils.php';
require_once 'config.php';

$delta = 0;
$id = "";
$field = "";
$options = getopt("i:f:");

if ( isset($options['i']) )
{
    $id = $options['i'];
}
else
{
    error_log( "ID is mandatory" );
    exit(1);
}

if ( isset($options['f']) )
{
    $field = $options['f'];
}
else
{
    error_log( "Field is mandatory" );
    exit(2);
}

//App client configuration
$scope = Netatmo\Common\NAScopes::SCOPE_READ_STATION;
$config = array("client_id" => $client_id,
                "client_secret" => $client_secret,
                "username" => $test_username,
                "password" => $test_password);

$client = new Netatmo\Clients\NAWSApiClient($config);

//Authentication with Netatmo server (OAuth2)
try
{
    $tokens = $client->getAccessToken();
}
catch(Netatmo\Exceptions\NAClientException $ex)
{
    handleError("An error happened while trying to retrieve your tokens: " .$ex->getMessage()."\n", TRUE);
}

//Retrieve user's Weather Stations Information

try
{
    //retrieve all stations belonging to the user, and also his favorite ones
    $data = $client->getData(NULL, TRUE);
}
catch(Netatmo\Exceptions\NAClientException $ex)
{
    handleError("An error occured while retrieving data: ". $ex->getMessage()."\n", TRUE);
}

if(empty($data['devices']))
{
    echo 'No devices affiliated to user';
}
else
{

    $device = $data['devices'][0];
    $tz = isset($device['place']['timezone']) ? $device['place']['timezone'] : "GMT";
    $date = new DateTime(null, new DateTimeZone($tz));
    $currentTimestamp = $date->getTimestamp();

    //devices are already sorted in the following way: first weather stations owned by user, then "friend" WS, and finally favorites stations. Still let's store them in different arrays according to their type
    foreach($data['devices'] as $device)
    {

        //favorites have both "favorite" and "read_only" flag set to true, whereas friends only have read_only
        if(isset($device['_id']) )
        {
            //echo $device['_id'] . " -- " . $id . "\n";
            if ($device['_id'] == $id ) {
                //echo "Station ID OK\n";
                //var_dump($device);
                if ( $field == 'time_utc' )
                {
                    if (isset($device['dashboard_data']) && isset($device['dashboard_data']['time_utc']))
                        $delta = $currentTimestamp - $device['dashboard_data']['time_utc'];
                        echo $delta . "\n";
                }                    
                elseif ( isset($device[$field]) )
                    echo "$device[$field]\n";
            } else {
                if(isset($device['modules']))
                {
                    foreach($device['modules'] as $module)
                    {
                        if(isset($module['_id']) && $module['_id'] == $id )
                        {
                            if ( $field == 'time_utc' )
                            {
                                if (isset($module['dashboard_data']) && isset($module['dashboard_data']['time_utc']))
                                    $delta = $currentTimestamp - $module['dashboard_data']['time_utc'];
                                    echo $delta . "\n";
                            }                    
                            elseif ( isset($module[$field]) )
                                echo "$module[$field]\n";
                        }
                    }
                }
            }
        }
    }

}
?>
